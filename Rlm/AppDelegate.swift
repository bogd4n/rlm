//
//  AppDelegate.swift
//  Rlm
//
//  Created by Bogdan Novikov on 11/06/2019.
//  Copyright © 2019 Generation Z Inc. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

