//
//  ItemsTVC.swift
//  Rlm
//
//  Created by Bogdan Novikov on 11/06/2019.
//  Copyright © 2019 Generation Z Inc. All rights reserved.
//

import RealmSwift

class ItemsTVC: UITableViewController {
    
    var group: Group?
    private var token: NotificationToken?
    
    deinit {
        token?.invalidate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = 60
        title = group?.name
        token = group?.items.observe({ [weak self] (changes) in
            switch changes {
            case .initial(_):
                self?.tableView.reloadData()
            case .update(_, deletions: let del, insertions: let add, modifications: let upd):
                self?.tableView.beginUpdates()
                self?.tableView.deleteRows(at: del.map{ IndexPath(row: $0, section: 0) }, with: .automatic)
                self?.tableView.insertRows(at: add.map{ IndexPath(row: $0, section: 0) }, with: .automatic)
                self?.tableView.reloadRows(at: upd.map{ IndexPath(row: $0, section: 0) }, with: .none)
                self?.tableView.endUpdates()
            case .error(let error):
                assertionFailure(error.localizedDescription)
            }
        })
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return group?.items.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.viewWithTag(1)?.backgroundColor = group?.items[indexPath.row].uiColor
        (cell.viewWithTag(2) as? UILabel)?.text = group?.items[indexPath.row].name
        return cell
    }
}

