//
//  GroupsTVC.swift
//  Rlm
//
//  Created by Bogdan Novikov on 11/06/2019.
//  Copyright © 2019 Generation Z Inc. All rights reserved.
//

import RealmSwift

class GroupsTVC: UITableViewController {

    private lazy var realm = try? Realm()
    private lazy var groups = realm?.objects(Group.self)
    private var token: NotificationToken?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = 60
        print("Realm URL:", realm!.configuration.fileURL!)
        token = groups?.observe({ [weak self] (changes) in
            switch changes {
            case .initial(_):
                self?.tableView.reloadData()
            case .update(_, deletions: let del, insertions: let add, modifications: let upd):
                self?.tableView.beginUpdates()
                self?.tableView.deleteRows(at: del.map{ IndexPath(row: $0, section: 0) }, with: .automatic)
                self?.tableView.insertRows(at: add.map{ IndexPath(row: $0, section: 0) }, with: .automatic)
                self?.tableView.reloadRows(at: upd.map{ IndexPath(row: $0, section: 0) }, with: .none)
                self?.tableView.endUpdates()
            case .error(let error):
                assertionFailure(error.localizedDescription)
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let destVC = segue.destination as? ItemsTVC {
            destVC.group = sender as? Group
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let items = [("Red One", "red"), ("The blue", "blue"), ("Orange?", "yellow"), ("Wtf??", "cyan")].enumerated().map { Item(id: $0.offset, name: $0.element.0, color: $0.element.1)
        }
        
        ["Group 1", "Group 2"].enumerated().forEach { item in
            try? realm?.write {
                let group = Group(id: item.offset, name: item.element)
                group.items.append(objectsIn: items)
                realm?.add(group, update: .modified)
            }
        }
    }
    
    @IBAction func randomPressed(_ sender: Any) {
        let item = groups?.filter("id == 0").first
        try? realm?.write {
            item?.name = "Some random name: " + String(Int.random(in: 0...1000))
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.viewWithTag(1)?.backgroundColor = groups?[indexPath.row].items.first?.uiColor
        (cell.viewWithTag(2) as? UILabel)?.text = groups?[indexPath.row].name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "open", sender: groups?[indexPath.row])
    }
}

