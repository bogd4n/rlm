//
//  RealmModels.swift
//  Rlm
//
//  Created by Bogdan Novikov on 11/06/2019.
//  Copyright © 2019 Generation Z Inc. All rights reserved.
//

import RealmSwift

final class Item: Object {
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc private dynamic var color: String?
    
    let groups = LinkingObjects(fromType: Group.self, property: "items")
    
    convenience init(id: Int, name: String, color: String) {
        self.init()
        self.id = id
        self.name = name
        self.color = color
    }
}

final class Group: Object {
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    var items = List<Item>()
    
    convenience init(id: Int, name: String) {
        self.init()
        self.id = id
        self.name = name
    }
}






// Computed properties
extension Item {
    var uiColor: UIColor {
        switch color ?? "" {
        case "red": return .red
        case "blue": return .blue
        case "yellow": return .yellow
        case "cyan": return .cyan
        default: return .black
        }
    }
}
